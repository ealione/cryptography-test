# -*- coding: utf-8 -*-

from re_encrypt import encrypt, re_encrypt, decrypt
from re_encrypt import keygen
from key_shares import make_random_shares
from key_shares import recover_secret

PRIME = 2 ** 127 - 1
MINIMUM = 3
SHARES = 6


vote_list = [1, 2, 1, 4]
print("vote list is %s" % vote_list)

p, g, x, y = keygen(120, 1)

shares = make_random_shares(p, minimum=MINIMUM, shares=SHARES)
print("shares are %s" % shares[:MINIMUM])

print("p is %s" % p)
new_p = recover_secret(shares[:MINIMUM])

print("new_p is %s" % new_p)

vote_list = [encrypt(new_p, g, y, e) for e in vote_list]

vote_list = [re_encrypt(new_p, e) for e in vote_list]

final_votes = [decrypt(e, x, p)[0] for e in vote_list]
print("final vote list is %s" % final_votes)
